import express from 'express';
import { compare } from 'bcrypt';
import { createUser, getUserByEmail, generateAccessToken } from './auth.services';

const router = express.Router();
export default router;

router.post('/signup', async (req, res, next) => {
    try {
        const { email, password, name } = req.body;
        if (!email || !password || !name) {
            res.status(400);
            throw new Error('invalid request data.');
        }

        const existingUser = await getUserByEmail(email);

        if (existingUser) {
            res.status(400);
            res.send({
                message: 'email already exists',
            });
            throw new Error('email already exists');
        }

        const user = await createUser({
            email,
            name,
            password,
        });
        const accessToken = generateAccessToken(user);

        res.json({
            name: user.name,
            email: user.email,
            accessToken,
        });
    } catch (err) {
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            res.status(400);
            throw new Error('invalid request data.');
        }

        const user = await getUserByEmail(email);

        if (!user) {
            res.status(401);
            throw new Error('user not found.');
        }

        const validPassword = await compare(password, user.password);
        if (!validPassword) {
            res.status(403);
            throw new Error('invalid login credentials.');
        }

        const accessToken = generateAccessToken(user);

        res.json({
            name: user.name,
            email: user.email,
            accessToken,
        });
    } catch (err) {
        next(err);
    }
});
