import { ReactNode, useCallback, useMemo, useState } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import AuthContext from './Auth.context';
import { IAuthContext } from './Auth.types';

type IAuthProvider = {
    children: ReactNode;
};

const API_AUTHENTICATION_KEY = 'authorization';

function AuthProvider(props: IAuthProvider) {
    const { children } = props;

    const [user, setUser] = useState<IAuthContext['user']>();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<IAuthContext['error']>();

    const handlerOnReqSuccess = (accessToken: string, name: string, email: string) => {
        const token = `Bearer ${accessToken}`;
        Cookies.set(API_AUTHENTICATION_KEY, token);
        axios.defaults.headers[API_AUTHENTICATION_KEY] = token;
        setUser({
            name,
            email,
        });
    };

    const signUp = useCallback(async (email: string, password: string, name: string) => {
        try {
            setLoading(true);
            setError(undefined);
            const userReq = await axios.post('/auth/signup', {
                email,
                password,
                name,
            });

            handlerOnReqSuccess(userReq.data.accessToken, userReq.data.name, userReq.data.email);
        } catch (err) {
            if (err instanceof Error) setError(err);
        } finally {
            setLoading(false);
        }
    }, []);

    const login = useCallback(async (email: string, password: string) => {
        try {
            setLoading(true);
            setError(undefined);
            const userReq = await axios.post('/auth/login', {
                email,
                password,
            });
            handlerOnReqSuccess(userReq.data.accessToken, userReq.data.name, userReq.data.email);
        } catch (err) {
            if (err instanceof Error) setError(err);
        } finally {
            setLoading(false);
        }
    }, []);

    const logout = useCallback(() => {
        Cookies.remove(API_AUTHENTICATION_KEY);
        setUser(undefined);
        window.location.href = '/signin';
    }, []);

    const value = useMemo<IAuthContext>(
        () => ({
            user,
            loading,
            error,
            login,
            logout,
            signUp,
        }),
        [user, loading, error],
    );

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export default AuthProvider;
