# simple-todo-app

## developers

### Engines

- node: `^19.0.0`
- npm: `^9.0.0`

You can [install nvm](https://github.com/nvm-sh/nvm) that allows you to quickly install and use different versions of node via the command line.

### Install

from root

```bash
npm ci
```

### envs

Both client and server have an _.env.example_ file, you need to copy them into an _.env_ file, open them for more information.

### database

If you use a local database (mysql) you can set the database endpoint in the .env file, otherwise you can launch it via docker with the command

```bash
docker compose up
```

### web-ui

```bash
cd packages/web-ui
npm run dev
```

### server

```bash
cd packages/server
npm run migrate
npm run dev
```

## things I haven't completed

**server**

- jwt authentication does not have token refresh logic.
- requests have no middleware that validates the parameters.
- responses don't have a mapper, the whole entity is returned.
- error handling is not centralized, there are parts of code with duplicate checks.
- db events were not handled (connection error, disconnection...).
- the API that allows you to move tasks between groups is missing
- the API that allows you to update group names is missing
- the logic to handle the "deadline" task is missing
- I haven't had time to develop unit tests

**client**

Unfortunately I didn't have time to create some well done components, there is the postman file for the apis that have not been implemented.

- there is no separation logic of components (there are no performance optimizations in react rendering).
- most of the API have not been implemented.
- I haven't had time to implement webworkers to increase performance and manage the offline mode.
