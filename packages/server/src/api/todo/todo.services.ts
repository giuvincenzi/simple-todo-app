import type { User, Todo, TodoGroup } from '@prisma/client';
import db from '../../utils/db';

export const getTodo = (id: Todo['id']) =>
    db.todo.findUnique({
        where: {
            id,
        },
    });

export const getTodos = (userId: User['id']) =>
    db.todoGroup.findMany({
        where: {
            userId,
        },
        include: {
            todos: true,
        },
    });

export const getTodoGroup = (id: TodoGroup['id']) =>
    db.todoGroup.findUnique({
        where: {
            id,
        },
    });

export const createTodo = (todo: Pick<Todo, 'description' | 'todoGroupId'>) =>
    db.todo.create({
        data: {
            ...todo,
            status: 'PENDING',
        },
    });

export const createTodoGroup = (group: Omit<TodoGroup, 'id'>) =>
    db.todoGroup.create({
        data: group,
    });

export const updateTodoDescription = ({ id, description }: Pick<Todo, 'id' | 'description'>) =>
    db.todo.update({
        where: {
            id,
        },
        data: {
            description,
        },
    });

export const updateTodoStatus = ({ id, status }: Pick<Todo, 'id' | 'status'>) =>
    db.todo.update({
        where: {
            id,
        },
        data: {
            status,
        },
    });

export const deleteTodo = ({ id }: Pick<Todo, 'id'>) =>
    db.todo.delete({
        where: {
            id,
        },
    });
