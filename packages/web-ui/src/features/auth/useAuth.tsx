import { useContext } from 'react';
import { IAuthContext } from './Auth.types';
import AuthContext from './Auth.context';

const useAuth = (): IAuthContext => {
    const context = useContext(AuthContext);

    if (!context) {
        throw new Error('useAuth must be used within the AuthProvider');
    }

    return context;
};

export default useAuth;
