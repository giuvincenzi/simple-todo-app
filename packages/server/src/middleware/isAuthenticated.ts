import type { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import { JWT_SECRET } from '../api/auth/auth.services';

export default (req: Request, res: Response, next: NextFunction) => {
    const { authorization } = req.headers;

    if (!authorization) {
        res.status(401);
        throw new Error('Unauthorized');
    }

    try {
        const token = authorization.split(' ')[1];
        const payload = verify(token, JWT_SECRET) as Request['user'];
        req.user = payload;
    } catch (err) {
        res.status(403);
        throw new Error('Forbidden');
    }

    return next();
};
