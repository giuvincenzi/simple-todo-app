import express from 'express';
import {
    createTodo,
    createTodoGroup,
    deleteTodo,
    getTodo,
    getTodoGroup,
    getTodos,
    updateTodoDescription,
    updateTodoStatus,
} from './todo.services';

const router = express.Router();
export default router;

router.post('/create', async (req, res, next) => {
    try {
        const { description, todoGroupId } = req.body;

        if (todoGroupId) {
            const exist = await getTodoGroup(todoGroupId);
            if (!exist) {
                res.status(400);
                throw new Error('invalid todoGroupId.');
            }
        }

        await createTodo({
            description,
            todoGroupId: todoGroupId || req.user.defaultGroupId,
        });

        res.status(200);
        res.send();
    } catch (err) {
        next(err);
    }
});

router.post('/createGroup', async (req, res, next) => {
    try {
        const { name } = req.body;

        if (!name) {
            res.status(400);
            throw new Error('invalid group name.');
        }

        const createdGroup = await createTodoGroup({
            name,
            userId: req.user.id,
        });

        res.send(createdGroup);
    } catch (err) {
        next(err);
    }
});

router.get('/list', async (req, res, next) => {
    try {
        const todos = await getTodos(req.user.id);

        res.send(todos);
    } catch (err) {
        next(err);
    }
});

router.post('/updateDescription/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const { description } = req.body;

        if (!description) {
            res.status(400);
            throw new Error('invalid "description"');
        }

        if (!(await getTodo(Number(id)))) {
            res.status(400);
            throw new Error('todo not found');
        }

        const todoUpdated = await updateTodoDescription({ id: Number(id), description });

        res.send(todoUpdated);
    } catch (err) {
        next(err);
    }
});

router.post('/setPending/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        if (!(await getTodo(Number(id)))) {
            res.status(400);
            throw new Error('todo not found');
        }

        const todoUpdated = await updateTodoStatus({ id: Number(id), status: 'PENDING' });

        res.send(todoUpdated);
    } catch (err) {
        next(err);
    }
});

router.post('/setDone/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        if (!(await getTodo(Number(id)))) {
            res.status(400);
            throw new Error('todo not found');
        }

        const todoUpdated = await updateTodoStatus({ id: Number(id), status: 'DONE' });

        res.send(todoUpdated);
    } catch (err) {
        next(err);
    }
});

router.delete('/delete/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        if (!(await getTodo(Number(id)))) {
            res.status(400);
            throw new Error('todo not found');
        }

        await deleteTodo({ id: Number(id) });

        res.send();
    } catch (err) {
        next(err);
    }
});
