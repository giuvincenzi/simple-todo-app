import { RouteObject, Navigate, Outlet } from 'react-router-dom';
import Cookies from 'js-cookie';
import App from '../App';

const AuthCheck = () => {
    const token = Cookies.get('authorization');
    // TODO: check if token is valid
    return !token ? <Navigate to="signin" /> : <Outlet />;
};

const privateRoutes: RouteObject = {
    element: <AuthCheck />,
    children: [
        {
            path: '/',
            element: <App />,
        },
    ],
};

export default privateRoutes;
