import { AxiosError } from 'axios';

type User = {
    name: string;
    email: string;
};

export type IAuthContext = {
    user: User | null | undefined;
    loading: boolean;
    error: Error | AxiosError | undefined;
    login: (email: string, password: string) => Promise<void>;
    signUp: (email: string, password: string, name: string) => Promise<void>;
    logout: VoidFunction;
};
