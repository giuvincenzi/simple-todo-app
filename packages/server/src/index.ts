import express, { Express, Response } from 'express';
import dotenv from 'dotenv';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import methodOverride from 'method-override';
import isAuthenticated from './middleware/isAuthenticated';
import authRoutes from './api/auth/auth.routes';
import todoRoutes from './api/todo/todo.routes';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

// set secure headers
app.use(helmet());

// parsers
app.use(express.json({ limit: '25mb' }));
app.use(express.urlencoded({ extended: true }));

// gzip compression
app.use(compression());

// for use HTTP verbs such as PUT or DELETE
app.use(methodOverride());

// cors
app.use(cors());
app.options('*', cors());

app.get('/', (req, res: Response) => res.send());

// mount routes
app.use('/auth', authRoutes);
app.use('/todo', isAuthenticated, todoRoutes);

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
