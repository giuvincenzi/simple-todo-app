import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import {
    AppBar,
    Box,
    Button,
    Container,
    CssBaseline,
    Stack,
    ThemeProvider,
    Toolbar,
    Typography,
} from '@mui/material';
import defaultTheme from './theme/defaultTheme';
import GroupList from './components/GroupList';
import useAuth from './features/auth/useAuth';

function App() {
    const { logout, user } = useAuth();

    return (
        <ThemeProvider theme={defaultTheme}>
            <CssBaseline />
            <Container maxWidth="xl">
                <Box height="90vh" p={2}>
                    <Stack alignItems="center">
                        <AppBar position="static">
                            <Toolbar>
                                <Stack direction="row" spacing={1}>
                                    <Typography>{user?.name}</Typography>
                                    <Typography>{user?.email}</Typography>
                                </Stack>
                                <Button onClick={logout}>logout</Button>
                            </Toolbar>
                        </AppBar>
                        <GroupList />
                    </Stack>
                </Box>
            </Container>
        </ThemeProvider>
    );
}

export default App;
