import { Box, Button, Stack, TextField, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import useAuth from '../features/auth/useAuth';

function Login() {
    const navigate = useNavigate();
    const { login, loading, error, user } = useAuth();

    const [usernameInput, setUsernameInput] = useState<string>();
    const [passwordInput, setPasswordInput] = useState<string>();

    useEffect(() => {
        if (!user && !error) return;

        if (user) {
            navigate('/');
        }
    }, [error, user]);

    return (
        <Stack height="100vh" alignItems="center" justifyContent="center" spacing={2}>
            <Typography variant="h3">Sign In</Typography>
            <TextField
                label="email"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setUsernameInput(e.target.value);
                }}
                sx={{
                    width: 250,
                }}
            />
            <TextField
                label="password"
                type="password"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setPasswordInput(e.target.value);
                }}
                sx={{
                    width: 250,
                }}
            />
            {loading && <Box>Loading...</Box>}
            {error && <Box color="error.primary">{error.message}</Box>}
            <Stack direction="row">
                <Button
                    onClick={() => {
                        if (!usernameInput || !passwordInput) return;
                        login(usernameInput, passwordInput);
                    }}
                >
                    Login
                </Button>
                <Button
                    onClick={() => {
                        navigate('/signup');
                    }}
                >
                    Go to signup
                </Button>
            </Stack>
        </Stack>
    );
}

export default Login;
