/* eslint-disable import/prefer-default-export */
import { RouteObject } from 'react-router-dom';
import SignUp from '../components/SignUp';
import Login from '../components/Login';

const publicRoutes: RouteObject = {
    children: [
        {
            path: 'signin',
            element: <Login />,
        },
        {
            path: 'signup',
            element: <SignUp />,
        },
    ],
};

export default publicRoutes;
