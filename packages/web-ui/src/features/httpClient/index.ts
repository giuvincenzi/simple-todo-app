import { configure } from 'axios-hooks';
import Axios, { AxiosError } from 'axios';
import Cookies from 'js-cookie';

const baseURL = import.meta.env.VITE_API_BASE_URL;

const handle401Error = (error: AxiosError) => {
    if (error.response?.status === 401) {
        window.location.href = '/signin';
    }

    return Promise.reject(error);
};

Axios.defaults.baseURL = baseURL;
Axios.defaults.headers.authorization = Cookies.get('authorization') as string;
Axios.interceptors.response.use(
    response => response,
    error => handle401Error(error),
);

configure({ axios: Axios });
