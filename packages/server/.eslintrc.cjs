module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'airbnb',
        'airbnb-typescript',
        'plugin:@typescript-eslint/recommended',
        'plugin:import/errors',
        'plugin:import/recommended',
        'plugin:import/typescript',
        'plugin:import/warnings',
        'prettier',
    ],
    parser: '@typescript-eslint/parser',
    ignorePatterns: [
        'node_modules',
        'playground',
        'test',
        'dist',
        'vite.config.js',
        '.eslintrc.js',
        'jest.config.js',
        'prettier.config.js',
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 'latest',
        sourceType: 'module',
        project: 'tsconfig.eslint.json',
        tsconfigRootDir: __dirname,
    },
    plugins: ['@typescript-eslint/eslint-plugin', 'simple-import-sort', 'prettier'],
    rules: {
        'prettier/prettier': 'error',
        'unicode-bom': 'error',
        'eol-last': 'warn',
        'consistent-return': 'off',
        '@typescript-eslint/no-use-before-define': [
            'error',
            {
                functions: false,
                classes: true,
                variables: true,
                allowNamedExports: false,
            },
        ],
    },
};
