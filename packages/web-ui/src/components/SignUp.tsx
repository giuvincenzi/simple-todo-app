import { Box, Button, Stack, TextField, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import useAuth from '../features/auth/useAuth';

function SignUp() {
    const navigate = useNavigate();
    const { user, loading, signUp, error } = useAuth();

    const [name, setName] = useState<string>();
    const [email, setEmail] = useState<string>();
    const [password, setPassword] = useState<string>();

    useEffect(() => {
        if (!user && !error) return;

        if (user) {
            navigate('/');
        }
    }, [error, user]);

    return (
        <Stack height="100vh" alignItems="center" justifyContent="center" spacing={2}>
            <Typography variant="h3">Sign Up</Typography>
            <TextField
                label="name"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setName(e.target.value);
                }}
                sx={{
                    width: 250,
                }}
            />
            <TextField
                label="email"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setEmail(e.target.value);
                }}
                sx={{
                    width: 250,
                }}
            />
            <TextField
                label="password"
                type="password"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setPassword(e.target.value);
                }}
                sx={{
                    width: 250,
                }}
            />
            {loading && <Box>Loading...</Box>}
            {error && <Box color="error.primary">{error.message}</Box>}
            <Stack direction="row">
                <Button
                    onClick={() => {
                        if (!email || !password || !name) return;
                        signUp(email, password, name);
                    }}
                >
                    SignUp
                </Button>
                <Button
                    onClick={() => {
                        navigate('/signin');
                    }}
                >
                    Go to login
                </Button>
            </Stack>
        </Stack>
    );
}

export default SignUp;
