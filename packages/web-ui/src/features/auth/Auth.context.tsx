import { createContext } from 'react';
import { IAuthContext } from './Auth.types';

const AuthContext = createContext<IAuthContext | null>(null);

export default AuthContext;
