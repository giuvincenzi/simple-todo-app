import { Button, Grid, Stack, TextField, Typography } from '@mui/material';
import useAxios from 'axios-hooks';
import { useEffect, useState } from 'react';

// todo: take this type from shared package types
type IGroupListData = {
    id: number;
    name: string;
    todos: {
        id: number;
        description: string;
        status: string;
    }[];
};

function GroupList() {
    const [{ data, loading, error }, refetch] = useAxios<IGroupListData[]>('/todo/list');
    const [newGroup, setNewGroup] = useState<string>('');

    const [
        { data: addGroupResp, loading: loadingGroupReq /* TODO: handle errors */ },
        addGroupReq,
    ] = useAxios<IGroupListData[]>(
        {
            method: 'POST',
            url: '/todo/createGroup',
            data: {
                name: newGroup,
            },
        },
        { manual: true },
    );

    useEffect(() => {
        if (!addGroupResp) return;
        setNewGroup('');
        refetch();
    }, [addGroupResp]);

    if (loading) {
        return <>loading...</>;
    }

    return !data ? null : (
        <Stack width={1}>
            <Stack spacing={0.5} width={300}>
                <TextField
                    value={newGroup}
                    onChange={e => setNewGroup(e.target.value)}
                    label="Add new Group"
                />
                <Button
                    disabled={newGroup.length < 2 || loadingGroupReq}
                    variant="contained"
                    onClick={() => {
                        addGroupReq();
                    }}
                >
                    add
                </Button>
            </Stack>
            <Stack direction="row" overflow="auto" width={1}>
                {data.map(group => (
                    /* TODO: split in separate component */
                    <Stack bgcolor="grey.900" p={2} spacing={2} m={1} key={group.id}>
                        <Stack spacing={1} minWidth={500}>
                            <Typography variant="h4">{group.name}</Typography>
                            <Stack direction="row" spacing={0.5}>
                                <TextField label="Add new task" />
                                <Button variant="contained">Add (not implemented)</Button>
                            </Stack>
                        </Stack>
                        {group.todos.map(todo => (
                            /* TODO: split in separate component */
                            <Stack
                                key={todo.id}
                                direction="row"
                                justifyContent="space-between"
                                bgcolor="grey.800"
                                p={1}
                                borderRadius={1}
                            >
                                <Typography variant="subtitle1">{todo.description}</Typography>
                                <Button color="warning">edit (not implemented)</Button>
                            </Stack>
                        ))}
                    </Stack>
                ))}
            </Stack>
        </Stack>
    );
}

export default GroupList;
