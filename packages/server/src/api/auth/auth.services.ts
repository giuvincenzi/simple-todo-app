import type { User } from '@prisma/client';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import db from '../../utils/db';
import { createTodoGroup } from '../todo/todo.services';

// TODO: move in .env
export const JWT_SECRET =
    'eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTY4MDIzMTY3MiwiaWF0IjoxNjgwMjMxNjcyfQ.ZQo9TobFq7YSXCy7ellV0LQPSmAVcSyd6i_FvhQapto';

export const generateAccessToken = (user: User) =>
    jwt.sign({ id: user.id, defaultGroupId: user.defaultGroupId }, JWT_SECRET, {
        expiresIn: '30m',
    });

export const getUserByEmail = (email: User['email']) =>
    db.user.findUnique({
        where: {
            email,
        },
    });

export const createUser = async (user: Omit<User, 'id' | 'defaultGroupId'>) => {
    const userCreated = await db.user.create({
        data: {
            ...user,
            password: bcrypt.hashSync(user.password, 12),
        },
    });
    const defaultGroup = await createTodoGroup({
        name: 'backlog',
        userId: userCreated.id,
    });

    return db.user.update({
        where: {
            id: userCreated.id,
        },
        data: {
            defaultGroupId: defaultGroup.id,
        },
    });
};
