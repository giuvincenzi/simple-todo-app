declare namespace Express {
    interface Request {
        user: {
            id: number;
            defaultGroupId: number;
        };
    }
}
